from typing import Pattern
from pywebio import start_server
from pywebio.input import *
from pywebio.output import *
from pywebio.session import hold, go_app

import time,datetime,signal
import requests
from kiAFlib import kiAFlib
from kiAFlib import kiAFlib_motor_pattern



kINIT_Speed_POS = 750
kINIT_Speed_NEG = kINIT_Speed_POS * -1
kMAX_Speed = 1000
kMIN_Speed = kMAX_Speed * -1
kSTEP_Speed = 25

kmsec = 1000
kINIT_Period = 2
kINIT_Period_ms = kINIT_Period * kmsec

gLED_switch = 0

gStrManual = 'Manual'
gStrArc = "Arc"
gStrCircle = "Circle"
gStrRect = "Rectangle"
gStrDia = "Diamond"


###
### Task scheduling part
###
gTick = 0.1

g0500msec = 0.5
gCount0500msec = 0

g1000msec = 1
gCount1000msec = 0

g2000msec = 2
gCount2000msec = 0


def layout():
    put_html('<span style="font-size: 200%"; <strong>Rover controller</strong></span>')

    speed = input_group('Rover setting',[
        select('Action pattern:', [gStrManual, gStrArc, gStrCircle, gStrRect, gStrDia], name='pattern'),

        input('Left wheel speed: {} ~ {}'.format(kMIN_Speed, kMAX_Speed), name='FL_speed', type=NUMBER, placeholder='Front-Left speed:', value=kINIT_Speed_POS, step = kSTEP_Speed),
        input(name='RL_speed', type=NUMBER, placeholder='Rear-Left speed:', value=kINIT_Speed_POS, step = kSTEP_Speed),
        input('Right wheel speed: {} ~ {}'.format(kMIN_Speed, kMAX_Speed), name='FR_speed', type=NUMBER, placeholder='Front-Right speed:', value=kINIT_Speed_POS, step = kSTEP_Speed),
        input(name='RR_speed', type=NUMBER, placeholder='Rear-Right speed:', value=kINIT_Speed_POS, step = kSTEP_Speed),

        input('Period: Enter [msec]', name='period', type=NUMBER, placeholder='Move period:', value=kINIT_Period_ms)
    ])

    put_text(speed['pattern'])

    move(speed)



def move(aSpeed):
    put_text(aSpeed)
    if aSpeed['pattern'] != gStrManual:
        put_text('Pattern moving.')
        if aSpeed["pattern"] == gStrArc:
            kiAFlib_motor_pattern.moveArc(aSpeed['FL_speed'], aSpeed['period'] / kmsec)
        elif aSpeed['pattern'] == gStrRect:
            kiAFlib_motor_pattern.moveRect(aSpeed['FL_speed'], aSpeed['period'] / kmsec)
        elif aSpeed['pattern'] == gStrDia:
            kiAFlib_motor_pattern.moveDia(aSpeed['FL_speed'], aSpeed['period'] / kmsec)
    else:
        put_text('Manual moving')
        if aSpeed['FL_speed'] > kMIN_Speed and aSpeed['FR_speed'] < kMAX_Speed and aSpeed['RL_speed'] > kMIN_Speed and aSpeed['RR_speed'] < kMAX_Speed:
            put_text('Possible to move')
            put_text(aSpeed['FL_speed'])
            kiAFlib.motorDrive(0, aSpeed['FL_speed'])
            kiAFlib.motorDrive(1, aSpeed['RL_speed'])
            kiAFlib.motorDrive(2, aSpeed['FR_speed'])
            kiAFlib.motorDrive(3, aSpeed['RR_speed'])
            kiAFlib.motorExe()
            time.sleep(aSpeed['period'] / kmsec)

            kiAFlib.motorAllOff()

        else:
            put_text('Impossible to move')
    #
    # Motor Controll
    #
    put_text('Reload or Press [F5] after moving.')






def blink_LED():
    global gLED_switch
    if gLED_switch:
        kiAFlib.led(1)
    else:
        kiAFlib.led(0)

    gLED_switch = not gLED_switch


def task0500msec():
    blink_LED()
    return


def task1000msec():
    return


def task2000msec():
    return


#####
# インターバルタイマーハンドラー
def intervalHandler(signum, frame):
    global g0500msec, gCount0500msec, g1000msec, gCount1000msec, g2000msec, gCount2000msec

    """定期的に実行したい処理を書く"""
    if gCount0500msec >= g0500msec / gTick:
        gCount0500msec = 0
        task0500msec()
    elif gCount1000msec >= g1000msec / gTick:
        gCount1000msec = 0
        task1000msec()
    elif gCount2000msec >= g2000msec / gTick:
        gCount2000msec = 0
        task2000msec()

    gCount0500msec = gCount0500msec + 1
    gCount1000msec = gCount1000msec + 1
    gCount2000msec = gCount2000msec + 1


# インターバル起床するハンドラを設定(SIGALRMハンドラー)
signal.signal(signal.SIGALRM,intervalHandler)



if __name__ == '__main__':
    signal.setitimer(signal.ITIMER_REAL, 1, gTick)
    start_server(layout, port = 54282)
