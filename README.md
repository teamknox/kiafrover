# kiAFRover

Totally new environment for Rover driving with kikor for Groovy-IoT and Alt-FW for Groovy-Quatro

## Concept
 Some Rover driving applications are necessary for efficent driving. For instances such as autonomus (a.k.a. Self Driving, a part of AI/Machine learning) control and Interaction (remote) control. Controller applications (App), Control middle ware (MW), Firmware (FW) are should be loose-coupling for freely development independently. New MW and FW are developed details is below,

### kikori as MW
 kikori is Web API type Devices (e.g. UART, GPIO, AD/DA, etc.) handling server, developed by myst3m.

### Alt-FW (a.k.a. Alternative-FW for Groovy motor controllers)

## Deliverable
Developed Apps are Interaction-type as  app and Autonomous-type app are both web-browser based. Each app maps to each tab of browser. 

### kiAF_PyWeb_Rover.py
 kiAF_PyWeb_Rover.py is framework for autonomous driving system PyWebIO basis. So far, implemented shape-pattern (e.g. rectanble, diamanod, etc.) instead of autonomus functionality. 

### kiAF_Jupyter_Rover.ipynb
 kiAF_Jupyter_Rover.ipynb is developed by Jupyter-lab. Mainly, designed for remote-control driving (not self-driving). 

### kiAFlib
 kiAFlib is low-layer functions, designed for UART wrapper of AF. 

## How to execute
 kikori and PyWebIO are server system. Both are needed to execute bofore apps execution. 
 1. start kikori
 2. run kiAF_PyWeb_rover.py
 3. Open browser and map each app to each tab.
 
# License
Copyright (c) Osamu OHASHI  
Distributed under the MIT License either version 1.0 or any later version. 


 
