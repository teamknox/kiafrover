#!/usr/bin/python3
# -*- coding: utf-8 -*-

import time,datetime,signal
from . import kiAFlib

###
### Pattern: Arc
###
def moveArc(aSpeed, aPeriod):

    time.sleep(aPeriod)

    kiAFlib.motorAllOff()


###
### Pattern: Rectangle
###
def moveRect(aSpeed, aPeriod):
    kiAFlib.motorNEWS(0, aSpeed)    # North
    kiAFlib.motorExe()
    time.sleep(aPeriod)
    kiAFlib.motorNEWS(1, aSpeed)    # East
    kiAFlib.motorExe()
    time.sleep(aPeriod)
    kiAFlib.motorNEWS(2, aSpeed)    # South
    kiAFlib.motorExe()
    time.sleep(aPeriod)
    kiAFlib.motorNEWS(3, aSpeed)    # West
    kiAFlib.motorExe()
    time.sleep(aPeriod)

    kiAFlib.motorAllOff()


###
### Pattern: Diamond
###
def moveDia(aSpeed, aPeriod):
    kiAFlib.motorNEWS(7, aSpeed)    # NorthWest
    kiAFlib.motorExe()
    time.sleep(aPeriod)
    kiAFlib.motorNEWS(4, aSpeed)    # NorthEast
    kiAFlib.motorExe()
    time.sleep(aPeriod)
    kiAFlib.motorNEWS(5, aSpeed)    # SouthEast
    kiAFlib.motorExe()
    time.sleep(aPeriod)
    kiAFlib.motorNEWS(6, aSpeed)    # SouthWest
    kiAFlib.motorExe()
    time.sleep(aPeriod)

    kiAFlib.motorAllOff()
