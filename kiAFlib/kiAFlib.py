#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import time
import requests

strMoveCommand = "http://localhost:3000/write?target=QUATRO&string-data=cmd+"
exe = "http://localhost:3000/write?target=QUATRO&string-data=exe"

bmx055_request = "http://localhost:3000/write?target=QUATRO&string-data=bmx055"
bmx055_response = "http://localhost:3000/read?target=QUATRO&type=string"

pos_request = "http://localhost:3000/write?target=QUATRO&string-data=pos"
pos_response = "http://localhost:3000/read?target=QUATRO&type=string"

led_on = "http://localhost:3000/write?target=QUATRO&string-data=led+1&crlf=true"
led_off = "http://localhost:3000/write?target=QUATRO&string-data=led+0&crlf=true"


### NotWorked ###
def pos():
    r = requests.post(pos_request)
    r = requests.post(pos_request)
    r = requests.get(pos_response)
    sensors = r.json()
    pos_str = sensors["value"].splitlines()[0].split()
    pos_int = [int(s) for s in pos_str]
    return(pos_int)


### NotWorked ###
def bmx055():
    r = requests.post(bmx055_request)
    r = requests.post(bmx055_request)
    r = requests.get(bmx055_response)
    sensors = r.json()
    bmx055_str = sensors["value"].splitlines()[0].split()
    bmx055_int = [int(s) for s in bmx055_str]
    return(bmx055_int)


def led(aOnOff):
    if aOnOff:
        r = requests.post(led_on)
    else:
        r = requests.post(led_off)




#############################################################################3

###
def motorExe():
    r = requests.post(exe)


###
def motorDrive(aMotor, aSpeed):
    url = strMoveCommand + str(aMotor) + "+01+" + str(aSpeed)
    r = requests.post(url)


###
def motorCW(aSpeed):
    motorDrive(0, aSpeed)
    motorDrive(1, aSpeed)
    motorDrive(2, -aSpeed)
    motorDrive(3, -aSpeed)
#    r = requests.post(exe)


###
def motorCCW(aSpeed):
    motorDrive(0, -aSpeed)
    motorDrive(1, -aSpeed)
    motorDrive(2, aSpeed)
    motorDrive(3, aSpeed)
#    r = requests.post(exe)


###
def motorNEWS(aNEWS, aSpeed):
    listNorth = [1, 1, 1, 1]        #0
    listEast = [1, -1, -1, 1]       #1
    listSouth = [-1, -1, -1, -1]    #2
    listWest = [-1, 1, 1, -1]       #3
    listNE = [1, 0, 0, 1]           #4
    listSE = [0, -1, -1, 0]         #5
    listSW = [-1, 0, 0, -1]         #6
    listNW = [0, 1, 1, 0]           #7
    listMotorNEWS = [listNorth, listEast, listSouth, listWest, listNE, listSE, listSW, listNW]

    i = 0
    for x in listMotorNEWS[aNEWS]:
        realSpeed = aSpeed * x
        url = strMoveCommand + str(i) + "+01+" + str(realSpeed)
        r = requests.post(url)
        i = i + 1

#    r = requests.post(exe)


###
def motorAllOff():
    motorDrive(0, 0)
    motorDrive(1, 0)
    motorDrive(2, 0)
    motorDrive(3, 0)
    motorExe()
